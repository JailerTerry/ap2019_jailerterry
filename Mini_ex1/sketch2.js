let x=0,y=0;
let img;
function preload(){
  imgFiltered=loadImage("Ronweasly(2).jpg")
  imgnotfiltered=loadImage("Ronweasly(2).jpg");
  img=imgnotfiltered;
  img2=loadImage("Wand.png")
  img3=loadImage("PROF Q.png")
}
function setup() {
  createCanvas(windowWidth,windowHeight);
  image(img,x,y);
  image(img2,10,20)
  image(img3,20,20)
  imgFiltered.filter("threshold", 0.5);
  angleMode(DEGREES);
}

var r = 0
function draw() {
  background(200,80,20,80);
  x=(x+1)%200;
  y=(y+1)%200;
  image(img,x,y);
  scale(0.5)
  image(img2,mouseX,mouseY);
  textSize(40);
  fill(0, 102, 153);
  text('Hi fellow wizards', 10, 60);
  fill(0, 102, 153, 80);
  text('Wanna see some magic?', 10, 90);

  rotate(r+=7);
  image(img3,800,500);
}
let b = false;
function mousePressed(){
  b = !b;
  if (b){
    img=imgFiltered;
  } else {
    img=imgnotfiltered;
  }

}
