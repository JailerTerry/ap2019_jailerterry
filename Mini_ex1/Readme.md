[link](https://cdn.staticaly.com/gl/JailerTerry/ap2019_jailerterry/raw/master/Mini_ex1/index2.html)
![screenshot](https://gitlab.com/JailerTerry/ap2019_jailerterry/blob/master/Mini_ex1/MINI%20EX1%20sk%C3%A6rmbillede.JPG)

In the first mini excercise, i have played around with different kinds of syntax relating to images, which will also seem
pretty obvious with the many different things moving around on the screen when you load the program. Though it might seem like nonsense,
the wizard theme reflects the way i feel about programming right now, it is a new world similar to that of magic.
The paralel is really that, whenever i learn to type out a certain syntax or understand the way variables work it
somewhat resembles what it would be like learning magic: You chant out some weird
cryptic sentence and maybe something sticks, and you get to see the magic unfold on your screen.
If it donsn't work, you won't necesarily know why, then maybe try out putting in that ";"" or "}" 
or maybe the last three A's in function leviosa() should have been capital.

In my mini_ex1 i have mainly focused on trying out uploading and displaying images on the screen, and afterwords making them do something.
I have tried out some different types of syntax and variables and tried to make images rotate, slide and react to the mouses' placement.

In the link you will see that the program isn't very interesting in it self, but behind it lies a lot of confusion and experimenting.
To me this excercise have been a lot of fun and a bit of frustration. Even the simplest tasks and commands proved to be harder than i thought.
Specifically the toggle-function between the the filter on the image:"Ronweasly(2).jpg", prooved  itself to be quite a complicated task,
to me atleast.
It was difficult in the sense, that wrapping my head around the process the computer reads and runs the code, first really made sense,
when i played around with variables, hoping to make a program that could be interacted with to some extent, and not only run in a linear fashion.

My biggest realization, within my first week of trying out writing my own code, has been 
how immensly and incredibly complex a simple program such as Paint must be. How seemingly primitive, and at the same time extremely brilliant 
the computer is, and how huge the possibilities are with such a domain and tool. 
It suddenly really makes sense to talk about litteracy in context with coding, as i first now, after many years with daily computer-interactions, i have gotten a real idea of the vast and complicated world of coding,
actually gets to look behind the backdrop of the scene.




