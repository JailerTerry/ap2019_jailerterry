var x=[300,360,420]
var x1=[300,360,420]
var y=210;
//y1 array makes it so that the dots doesn't collide as often
var y1=[210,280,220,260]
var r=50;
var yDelta=-20;
var time=0;
var timeMax=0.38;
var t=[]

function setup(){
createCanvas(800,500);
frameRate(80)
}
function draw(){
  //Variables to control timing and movement of dots
  max=HALF_PI;
  steps=60*timeMax; //120
  stepsize=max/steps;
  time+=stepsize;
  t[0]=time
  t[1]=(time-0.7)
  t[2]=(time-1.4)
  background(80,130,240);
  //Draws falling dots
    push();
  //determines how fast the dots falls out of the frame
      var y1speed=[1,1.4,1.2,1.3,1.5,1.8,1.7]

      for(i=0; i<=7; i++){
      fill(180,150+sin(t[i]/HALF_PI)*40);
      ellipse(x1[i],y1[i],r)

        if(y1[i]<=600){
        y1[i]=y1[i]+y1speed[i];

        x1[i]=x1[i]+sin(t[i]);

        }
        else
          y1[i]=random(140,280)
      }
    pop();
//Draws bubble
  noStroke();
  fill(230);
  ellipse(240,260,100,100);
  ellipse(170,320,50,50)
  rect(200,100,320,200,100);
//Draws jumping dots
  for(i=0; i<=2; i++){
    push();
    if(sin(t[i]) < 0){
      fill(180,150);
      ellipse(x[i],y,r);
    } else {
      fill(180,150+sin(t[i])*105);
      ellipse(x[i],y+sin(t[i])*yDelta,r)
    }
    pop();
  }
}
