var flowers=[]
var flowerBud;

function setup(){
  createCanvas(windowWidth,windowHeight)
  flowers.push(new flower(100,100));
}
function draw(){
  colorMode(HSB, 360, 100, 100, 100);
  let backColor = color(200,80,50);
  background(backColor)
  for (let i=0; i <flowers.length; i++){
    flowers[i].show();
  }
}

class flower{
  constructor(x,y){
  this.y=y
  this.x=x
  this.d=8;
  this.r=random(PI);
  this.petals=[];
  for(let i=0; i <5;i++){
    this.petals.push(new petal(8*cos(i*2*PI/5),8*sin(i*2*PI/5),i*2*PI/5));
    }
  }
  show(){
    push();
    translate(this.x,this.y)
    rotate(this.r)
    let flowerBudColor=color(345,53,77,70);
    noStroke();75
    fill(flowerBudColor);
    this.petals.forEach(p => {
      p.show();
    })
    flowerBud=ellipse(0,0,this.d);
    pop();
    }
    update(){
    }
  }
function mousePressed(){
  flowers.push(new flower(mouseX,mouseY));

}
class petal{
  constructor(x,y,r){
    this.x=x;
    this.y=y
    this.u=random(9,11);
    this.p=random(7,9);
    this.r=r
    console.log(x,y,r);
  }
  show(){
    push();
    translate(this.x,this.y)
    rotate(this.r)

    let petal=color(342,25,79,70);
    fill(petal)
    petal=ellipse(0,0,this.u,this.p)
    pop();
  }
}
