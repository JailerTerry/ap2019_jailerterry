## Mini exercise 8
![A murder vicitm](Screenshot.png)

[link](https://glcdn.githack.com/JailerTerry/ap2019_jailerterry/raw/master/mini_ex8/index.html)

### Description
When creating this weeks mini_excercise Adam Naldal and i wanted to create a programming which built on the idea of giving the computer control of rewriting horror stories and experiment with the idea of the computers "perception" of language or lack thereof.
In relation to this concept, we chose to create the entire codework with a thematic reference to Frankenstein, by naming of variables as if the program acts as a machine that desicates and mutates human language, turning litterature into a thoughtlessly patched-together version of itself.

Firstly we created a JSON file made of 50 word horror stories that could be loaded and mutated by the program.
By pressing the button "Execute" the JSON file are seperated and analysed in strings using the RiTa library, which can detect and identify words and their classes.
When the texts are analysed, it recognises nouns and substitutes them with random nouns from the RiTa libraries lexicon. When new nouns has been substituted it gets the position of the words in the paragraph and prints a black box over them and the new words in a new red font on top of it, making the substitued words clearer for the reader.
All punctioation has been isolated in the JSON file as our program had problems with detecting nouns when followed by punctuation.
When the button "new victim" is pressed, the program chooses a random story from the json file and erases the previous text, narration boxes and red words.

We tried to make the program more evocative by adding features such as a narrator from the p5.speech library, ambient sounds and effects when pressing the individual buttons.

### Putting it into perspective
Working with Vocable code in this genre was particulary interesting as the computers inability to understand human language is displayed quite obviously.
The litterary genre of horror packs a certain "code", which can not so simply be executed by the computer, as it is isn't depending on a understanding of words or grammar, but a question of dramaturgy, anticipation, suspense and oral tradition.
The narrator also adds an interesting dimension to the program as it completely misses the mood and tradition of the genre, highlighting it's complete ignorance of actual theme and meaning of words.
As a main point the program is meant to point out how the computer perceives litterature, it only understands the math of it, the syntax and classifications of words, but it handles it as empty shells, no meaning is attached.
The program butchers the dramaturgical structure of horror stories, removing all suspense, creating a Frankenstein-like monster made up of human language, but even with human parts, the creation is nowhere near human.
the computer only understands so much of human language, limited to syntax and classification.

