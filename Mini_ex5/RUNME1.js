var followercount=0
var b1
var contentb;
var capture;
var canvas;
var img2;
var ad;

function preload(){
img2=loadImage("like.png");
}
function setup() {

  //sets up random number of friends.
  followercount = floor(random(600) + 200)
  select("#friendcounter").html("You have " + followercount + " facebook friends")
  //Sets up webcam/profile picture and a canvas on top, so that thumbs can print onto it when a fbButton is pressed.
  capture=createCapture(VIDEO)
  canvas=createCanvas(100,100)
  capture.attribute('width',100)
  capture.attribute('height',100)
  video=select("#capture")
  capture.parent(video)
  canvas.parent(video)
  contentb = select("#contentbody")
  generateContent();
  b1=select("#n1");
}
  //sets up content through dom-objects, adding to the html.
function generateContent(){
  //Gets images and names from online "API"
  inspurl="https://inspirobot.me/api?generate=true"
  nameurl="https://uinames.com/api/?ext&amount=25&region=united+states&gender=random&source=uinames.com"
  httpGet(nameurl, 'json', true, function(names) {
    // sets up loop that creates 10 different images, likebutton and names in the content-element. All stylings in style.css file and html
    for (var i = 0; i < 10; i++){
      let k = i;
      httpGet(inspurl, 'text', false, function(response) {
//creates the template where the pictures a posts are presented
        let div = createElement('div')
        div.addClass("content")
        div.parent(contentb)

//chooses a name from the uinames API
        let p = createElement('p')
        p.addClass("name")
        p.html(names[k].name + " " + names[k].surname)
        p.parent(div)
//chooses a image from the inspirobot API, simulating a post.
        let img = createElement('img')
        img.attribute('src', response)
        img.parent(div)
        img.addClass("contentimage")
//Creates "like button"
        let b = createButton('Like')
        b.addClass('fbbutton')
        b.parent(div)

//creates a liketext activated by clicking the fbbutton
        let liketext = createElement('p')
        liketext.addClass('liketext')
        liketext.parent(div)
        //when mouse is clicked a function is generated, typing out the liketext.
        b.mouseClicked(generateFunction(liketext))

      });
    }
  });
  //returns the generate content to the caller.
  return str;
}
//writes liketext underneath liketext
function generateFunction(element){
  let xVal=floor(random(0.9,2.3)*followercount)
  return function(){
    element.html("This like will be pushed to your profile and shared to aproxamitely " + floor(xVal/2.5) +" of your friends. Your likes value is " + (xVal/400)+ " $ Thank you for advertising our content.")
    //draws out thumb-images onto video-capture.
    image(img2,random(0,80),random(0,80),20,20)
  }
}
