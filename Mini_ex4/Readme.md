LINK(https://glcdn.githack.com/JailerTerry/ap2019_jailerterry/raw/master/Mini_ex4/index.html)
![screenshot](screenshot_min_ex4.JPG)

For this weeks assignment i experimented with the dom-library, CSS-styling and online datasets, which made
it possible to create a more interactable and stylistic program, than what p5.js could manage previously. 
Conceptually i have been many different uses of Video and Audio-Capture, but i ended up limiting myself a lot in terms of 
interactivity, and only used buttons. I originally had bigger conceptual plans for this Mini_ex, but as I started working 
through HTML and CSS it got sidetracked, as the new possibilities brought a lot of technical issues with it. 

The conceptuality of this program is centered around the idea of "the Like Economy", as presented in 
The like economy: Social buttons and the data-intensive web by Carolin Gerlitz and Anne Helmond.
The text explores how a social media expansion, originating through Facebook, uses a rhetoric of 
sociality and connectivity to turn the world wide web into a data farming machine, based on the transformation of 
the users interaction and activity into valuable consumer information.

In this age most users of the web are constantly distributing enormous amounts of data linked to their accounts, creating a
web were content and advertisements becomes targeted and  "personalized" for the specific user. Even though this sounds and might 
feel like a invasion of privacy, users are enabling this by willingly liking, sharing and allowing apps to monitor their activity, and through mobile apps,
like Snapchats map function, monitor their actual location through GPS.
Much of the problem is that there is very little transparency, given that all this data-distribution is hidden behind a "social" or practical feature, much like
how the Alexa is branded as a easy and smart helper in the daily life, it also listens in on everything going on the living room.
Building on these ideas, I tried to experiment with how a more transparent Facebook-like button would look, giving the user a 
feel of selling out every time they pressed it, knowing that every time they like something, it is just another way of promoting and 
advertising content on social media, by influencing your friends to do the same.
Unfortunately i couldnt find a more fitting API than the inspirobot to produce the posts, though they might not convey the conceptual point very well, it can be
also be seen as a placeholder for content/advertisements produced by profiles or Facebook pages that seek to make money off of publicity. 

DISCLAIMER: I didn't produce or author any of the "posts" in the program, they are all generated through: http://inspirobot.me/
