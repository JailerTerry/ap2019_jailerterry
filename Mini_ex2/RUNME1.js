//Preloads the images.
//The SVG file makes it posible to change fill and edit the code which draws the cars.
function preload(){
  svg=loadImage("Bil1.svg");
  svg2=loadImage("Bil10.svg");
}
//Creates Canvas and imageMode(CENTER) makes it possible to easily match the images to the center of the ellipse.
function setup(){
  createCanvas(windowWidth,10000);
  background(0);
  imageMode(CENTER);
  push();
  fill(170)
  rect(290,100,20,10000,4);
  pop();
  //Commands the drawSign fucntion to create multiple signs underneath eachother
  for(i = 1; i < 31; i++){
    drawSign(i*315)
  }
}
//Draws the '2019' road sign
function draw(){
  push();
  fill(170)
  rect(990,9250,20,10000,4);
  pop();
  fill(color="#eb0c00");
  strokeWeight(5);
  ellipse(1000,9450,300,300);
  fill(1000);
  ellipse(1000,9450,250,250);
  push();
  tint(color="#eb0c00");
  image(svg2,1000,9450,svg2.width*0.6,svg2.height*0.6);
  pop();
  push();
  tint(0,0,0);
  image(svg,1000,9450,svg.width*0.6,svg.height*0.6);
  pop();
  //Draws the text featured above the signs
  textSize(32);
  text('2035',100,60);
  text('2019',850,9150);
  fill(0, 102, 153);
  //the framerate setting makes it so that the program will run more smoothly.
  framerate(60);
}

//this function is run 31 times, each time choosing a random color for the left car
function drawSign(yPos){
  fill(color="#eb0c00");
  strokeWeight(5);
  ellipse(300,yPos,300,300);
  fill(1000);
  ellipse(300,yPos,250,250);
  push();
  tint(random(0,255),random(0,255),random(0,255));
  image(svg2,300,yPos,svg2.width*0.6,svg2.height*0.6);
  pop();
  push();
  tint(0,0,0);
  image(svg,300,yPos,svg.width*0.6,svg.height*0.6);
  pop();
}
