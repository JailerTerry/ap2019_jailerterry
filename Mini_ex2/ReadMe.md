![SCREENSHOT]https://gitlab.com/JailerTerry/ap2019_jailerterry/blob/master/Mini_ex2/RUNME%20SCREENSHOT.JPG

[link](https://cdn.staticaly.com/gl/JailerTerry/ap2019_jailerterry/raw/master/Mini_ex2/index.html)

My program is a comment on the evolution of emoticons/emojis, and how they have progressed from a symbolic meaning to a identity driven framework.
of options into the medium might be taking the emojis too far. I have been thinking about why and how this evolution has been
changing the way we communicate and identify ourselves through use of emojis.
Because of the prevalance of emojis that actually convey the same symbolic meaning, but with additional irellevant information, we could speculate
how emojis now works as a symbol, as it could be argued that these identity-driven emojis no longer creates room for the arbitrary nature of symbols.
It could be argued that these new standards co-created by users and tech-companies has opened a pandoras box, when it first started redefining the function of emojis.
We could speculate what has primed this devolepment but that is a larger and continuing debate. But with these new emoji-standards comes a whole new discussion
about how icons and symbols should be perceived. 

The main questions i raised working on this project was-  
What does this tendency say about our use of generalised symbols and icons?
For whom does it benefit having 20 versions of symbols for the same message we could easily convey with a single symbol? 

I have tried to take these questions out of context and tried to apply the same conditions of the latest emoji standards to my dystopian idea of a road sign in 2035.
Much like the earlier stages of the emoticon, the road sign is meant to convey a simple and 
easily identifable message through an icon or symbol. 
In my program i try to explore this concept by moving it into another medium using symbols and icons, and in the action conveying how this tendency could
dillute the purpose and efficiency of comunicating through simple icons and symbols.


