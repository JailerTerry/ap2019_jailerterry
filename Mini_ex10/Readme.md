## Mini_Ex10

**Individual**

[Mini_Ex7](https://gitlab.com/JailerTerry/ap2019_jailerterry/tree/master/mini_ex7)


![screenshot](Sol_7.png)


For the 7'th mini excercise, we worked with genereative art, and in our case we based our program of of Sol Lewitt's wall drawing #118.
Our itteration of this concept is much like most generative art, very non-linear and with no available interactions the flow chart
becomes quite short, given the few simple rules that it follows when running.
But still the flow chart effect can be useful as it simplifies what can seem somewhat complicated when seeing the program run or reading the code.
My issues with creating this flow chart was mainly focused around how and for what porpuse this concept could best be externalised in the flow chart format.
I often found myself overcomplicating the rules and relying too heavily on implementing the exact syntax used,
but i ended up returning to the simplest form i could, excluding most of the unneccesary information.
    
**Group**


![screenshot](RemieFlowchart.JPG)
   
   
         Difficulties
*         Difficulty with creating enough conceptual linkage to the themes of the course.
*         Bug fixing might take a lot of time with such a complicated program.
*         Requires a lot of graphical elements and possibly animation, we do not want to rely to heavily on text.
*         Balancing the gameplay. Should give the player a sense of progression but also increased difficulty.


    


![screenshot](MONO_NO_AWARE.JPG)
   
   
        Difficulties
*        We want to make a very slow experience but that is difficult to present in a reasonable time for an exam.
*        Generative art can be technically difficult or “mathy” to get right. And all of the plants expression has to come through the image that is generated.
*        The light detection library might be tricky, it plays as an essential part in the functionality of the program, but given that we have never worked with this          type of library before, we could end up with having to use a suboptimal alternative. 


    

**Individual**
How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?

The ideas presented in our group flow charts are substantially more complicated and expansive than the one representing Mini_ex7, which naturally ties to the level of ambtition, time and type of program that is presented in our groups flow charts.
The type of program that is presented in the group flow charts are two different takes on a management-game, which in contrast to a simple generative art program includes much more user engagement, dynamic interactions and progression. 
The first of our group flow charts portrays a "medieval miller" managagement game and given the many features and interaction-possibilities we envisioned, quite an extensive overview needed to be presented in order to make the concept clear. Though both group flow charts presents mangagement-games the "Flower management" game is less focused on the user possibilites and more engaged with the systems own process, making the two flow charts quite demanding in their own right. 



If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would
you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)

As it was proved in our class discussion on the 9th of april, flow charts and algorithms have multiple similarities. As all groups had to present a flow chart of a specific sorting algorithm-task, it was clear that, like Object oriented programming, there can be many different perceptions of how a specific task is completed. This is evident giving the many different algorithms shown to complete the same tasks, but expanding on this concept it is interesting to think about this aspect of algorithms as different people with different backgrounds, values and biasses engage in developing algorithms which control and engage on many levels in our society. Thinking about this in the context of the current importance and effect that algorithms have on both our digital culture, access to information and global society etc. It becomes evident that thinking of algorithms as a neutral cannot be very true, and that much like object orented programming there should be focus on how we perceive algorithms and who we hold accountable when the algorithms or AI systematically makes mistakes.