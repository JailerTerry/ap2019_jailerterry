var spr;
var cnv;
var boi;
var Block;
var blocks=[]
var blockY = 0;
var on=false;
var gif;
var score=0;

//Centers canvas in browser-page
function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

function setup() {
  cnv = createCanvas(600,400);
  centerCanvas();
  // gif=createImg('KREMIT.gif')

  boi = new boi();
  blocks.push(new Block());
}

function windowResized() {
  centerCanvas();
}
function draw(){
background(200);
if(frameCount>190){
this.score++
textSize(15);
text("Score: "+this.score,510,30)
}
on=false;
//creates for-loop to generate blocks,
for(var i=blocks.length-1;i>=1;i--){
  if(blocks[i].x<boi.x&&blocks[i].x+blocks[i].w>boi.x){
    on=true;
    blockY = blocks[i].top;
  }
blocks[i].show();
blocks[i].update();
//deletes blocks, so that there are only a 2 blocks in the program at a time
if (blocks[i].offscreen()){
  blocks.splice(i,1);
  }
}
//generates a new block depending on framecount.
  if(frameCount%85==0){
    blocks.push(new Block());
  }
  boi.update();
  boi.show();
}
//defines input for jump/up.
function keyPressed(){
  if(keyCode==UP_ARROW){
    boi.up();
  }

}
//defines the attributes of a block
function Block(){
  this.top=random(300,200);
  this.bottom=400
  this.x=width;
  this.w=random(140,300);
  this.speed=6;
  this.color=fill(210,100,40)
  // ()


//draws the colour
  this.show = function(){
    this.color;
    rect(this.x,this.top,this.w,this.bottom)
  }
    this.update=function(){
    this.x-=this.speed;
    //statement which checks position of boi in relation to x and y of blocks.
    if(boi.y>500||boi.y>this.top&&boi.x > this.x && boi.x < this.x + this.w){
      noLoop();
      fill(0)
      textAlign(CENTER);
      textSize(30)
      text("GAME OVER",300,100)
    }
    }
  this.offscreen=function(){
    if (this.x<-this.w){
      return true;
    } else{
      return false;
    }
  }
}
//defines attributes of boi
function boi(){
  this.y=height/2
  this.x=64;

  this.gravity=0.5;
  this.lift=-11.5;
  this.velocity=0;
  this.jumpcount = 0;
//draws boi
    this.show=function(){
      fill(10,20,100)
      ellipse(this.x, this.y-16,32,32);
      // gif.position(this.x,this.y)

    }
    //jump/up affects velocity. Sets jumpcount limit so that it is only possible to dubble jump
    this.up=function(){
      if (this.jumpcount < 2){
        this.velocity= this.lift+this.jumpcount*4;
        this.jumpcount++;
      }
    }
//sets boi to stop at the top of the block (blockY)
  this.update=function(){
      this.velocity+=this.gravity
      this.y+=this.velocity


      if(this.y>blockY){
        if (on){


        this.y=blockY;
        this.velocity=0;
        this.jumpcount=0;
      }
    }
    //More or less succesful way of limiting the possibility of jumping in mid air
    if(this.y>blockY+30){
      this.jumpcount++
    }

//in start of every itteration, the player starts in mid air
      if(frameCount<170){
        fill(100);
        textSize(30)
        text("Get ready! Press UP to jump",50,50)
        this.y=100;
        this.velocity=0;
        this.jumpcount=0;

      }


    }
  }
